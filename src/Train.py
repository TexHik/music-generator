import tensorflow as tf
import glob
import matplotlib.pyplot as plt
import numpy as np
import os
import PIL
import time
import sys
import DataHandling
import Neurals
import gc
np.set_printoptions(threshold=sys.maxsize)
WINDOW = 500
OFFSET = 250

BATCHSIZE = 1

generator = Neurals.GeneratorModel(WINDOW)
discriminator = Neurals.DiscriminatorModel(WINDOW)
print("Models created")


#discriminator = tf.keras.models.load_model('./discmodel.h5')
#generator = tf.keras.models.load_model('./genmodel.h5')




def train(epochs):
    print("Loading data")
    audio = DataHandling.getdata()
    print("Loaded audios: " + str(len(audio)))
    dataset = DataHandling.batchcreate(audio, WINDOW, OFFSET).reshape(-1, WINDOW)
    print("Data ready:", dataset.shape)

    for epoch in range(0,epochs):
        start = time.time()

        noise = np.array(tf.random.normal([BATCHSIZE, WINDOW]))
        genlosses = []
        disclosses = []
        for i in range(int(len(dataset)/BATCHSIZE)):
            print("Batch: "+str(i))
            data = np.array(dataset[i*BATCHSIZE:(i+1)*BATCHSIZE],dtype='float32')
            result,discloss,genloss = Neurals.trainstep(data, np.array(noise ,dtype='float32'), generator, discriminator)
            noise = np.append(noise,np.array(result)).reshape(BATCHSIZE,-1)[:,OFFSET:WINDOW+OFFSET]
            disclosses.append(discloss)
            genlosses.append(genloss)
        if (epoch%5==0):
            generator.save('./genmodel.h5')
            discriminator.save('./discmodel.h5')
        print ('Time for epoch {} is {} sec, loss: {}  {}'.format(epoch + 1, time.time()-start,np.asarray(genlosses).mean(),np.asarray(disclosses).mean()))
        #gc.collect()

def generate():

    noise = np.array(tf.random.normal([1, WINDOW]))
    data = np.zeros([0,WINDOW])
    for i in range(20):

        prediction = generator(noise)
        noise = np.append(noise, np.array(prediction)).reshape(1, -1)[:, OFFSET:WINDOW + OFFSET]

        data = np.append(data,noise)
    print("Done, saving")
    DataHandling.save(data.reshape(-1,1),"12")
    plt.plot(data)
    plt.show()
    print("Done")



train(120)
generate()