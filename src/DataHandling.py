import os
import numpy as np
from sklearn.preprocessing import MinMaxScaler
import subprocess as sp
import wave
FFMPEG_BIN = "./../ffmpeg"

def getdata():
    data = []
    for audio in os.listdir("./../dataset/input/"):
        command = [FFMPEG_BIN,
                   '-i', "./../dataset/input/"+audio,
                   '-f', 's16le',
                   '-acodec', 'pcm_s16le',
                   '-ar', '44100',  # ouput will have 44100 Hz
                   '-ac', '1',  # stereo (set to '1' for mono)
                   '-']
        pipe = sp.Popen(command, stdout=sp.PIPE, bufsize=10 ** 8)
        raw_audio = pipe.stdout.read(88200 * 4)
        pipe.terminate()
        datapart = np.array(np.fromstring(raw_audio, dtype="int16"), dtype="float64")
        k=0
        while (datapart[k]==0):
            k+=1
        datapart = datapart[k:]
        scaler = MinMaxScaler()
        scaler.fit(datapart.reshape(-1,1))
        data.append(scaler.transform(datapart.reshape(-1,1)))
    return data



def batchcreate(data, windowsize, offset):
    batches = []
    for audio in data:
        for j in range(int((len(audio) - windowsize)/offset)):
            batch = audio[j*offset:j*offset+windowsize]

            batches.append(batch.reshape(-1,1))
    return np.asarray(batches)


def save(data, name):
    scaler = MinMaxScaler(feature_range = (-30000, 30000))
    scaler.fit(data)
    data =scaler.transform(data)
    wav_file = wave.open("./dataset/output/"+name+".wav","w")
    wav_file.setparams((1, 2, 22050, len(data), "NONE", "not compressed"))
    wav_file.writeframes(data.reshape(-1))
    wav_file.close()
