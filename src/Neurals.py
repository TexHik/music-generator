import tensorflow as tf
from tensorflow.keras import layers
#from tensorflow.keras.utils import plot_model
import os
import numpy as np
os.environ["PATH"] += os.pathsep + 'C:/Users/TexHik/AppData/Local/Programs/Python/Python36/graph/bin'

def GeneratorModel(win):
    input = layers.Input(shape=(win,))

    l = layers.Reshape(target_shape=(win,1,))(input)

    l = layers.LSTM(win,return_sequences=True)(l)
    l = layers.LeakyReLU(0.5)(l)
    l = layers.LSTM(win, return_sequences=True)(l)
    l = layers.LeakyReLU(0.5)(l)
    l = layers.LSTM(win, return_sequences=False)(l)
    l = layers.LeakyReLU(0.5)(l)
    l = layers.Dropout(0.25)(l)
    out = layers.Dense(win)(l)

    model = tf.keras.Model(inputs=input,outputs=out)
    #plot_model(model, to_file='generator.png', show_shapes=True, show_layer_names=True)

    return model

def DiscriminatorModel(win):
    input = layers.Input(shape=(win,1,))

    l = layers.Reshape(target_shape=(win, 1,))(input)

    l = layers.LSTM(win, return_sequences=True)(l)
    l = layers.LeakyReLU(0.5)(l)
    l = layers.LSTM(win, return_sequences=True)(l)
    l = layers.LeakyReLU(0.5)(l)
    l = layers.LSTM(win, return_sequences=False)(l)
    l = layers.LeakyReLU(0.5)(l)
    l = layers.Dropout(0.25)(l)
    l = layers.Dense(win)(l)
    l = layers.LeakyReLU(0.5)(l)
    l = layers.Dropout(0.25)(l)
    out = layers.Dense(1,activation='relu')(l)


    model = tf.keras.Model(inputs=input, outputs=out)

    #plot_model(model, to_file='discriminator.png', show_shapes=True, show_layer_names=True)

    return model

cross_entropy = tf.keras.losses.BinaryCrossentropy(from_logits=True)



def generator_loss(fake_output):
    return cross_entropy(tf.ones_like(fake_output), fake_output)

def discriminator_loss(real_output, fake_output):
    real_loss = cross_entropy(tf.ones_like(real_output), real_output)
    fake_loss = cross_entropy(tf.zeros_like(fake_output), fake_output)
    total_loss = real_loss + fake_loss
    return total_loss




generator_optimizer = tf.keras.optimizers.Adam(1e-3)
discriminator_optimizer = tf.keras.optimizers.Adam(1e-3)




def trainstep(audio,noise, generator, discriminator):
    with tf.device('/cpu:0'):
        with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:

            new_generated_data = generator(noise, training=True)
            real_output = discriminator(audio, training=True)
            fake_output = discriminator(new_generated_data, training=True)

            gen_loss = generator_loss(new_generated_data)
            disc_loss = discriminator_loss(real_output, fake_output)

            gradients_of_generator = gen_tape.gradient(gen_loss, generator.trainable_variables)
            gradients_of_discriminator = disc_tape.gradient(disc_loss, discriminator.trainable_variables)

            generator_optimizer.apply_gradients(zip(gradients_of_generator, generator.trainable_variables))
            discriminator_optimizer.apply_gradients(zip(gradients_of_discriminator, discriminator.trainable_variables))

    return new_generated_data, float(disc_loss), float(gen_loss)
